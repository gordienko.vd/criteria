package criteria;

public interface Criteria {

    Criteria copy();
}
